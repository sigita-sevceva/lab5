Sigita Ševceva
Student ID: 204434IV 
Machine Learning Course (ICS0030-2020)
# Final Assignment
### Business problem
To create a machine learning model to train labeling positive versus negative film reviews. For this, a set of corpus of 10000 IMDB film reviews in the form of .txt files was used. The corpus is available here: http://ai.stanford.edu/~amaas/data/sentiment/
Adapted example was taken from a tutorial found here; however, as opposed to this example, only 3 models were chosen with several modifications [1].
### Chosen Approach
The field of Natural Language Processing (NLP) and its supporting toolkits to create a Python program to evaluate movie reviews and hence used adjectives into either positive or negative (using scalar/degree sentiment analysis) by training the program to label either ‘positive’ or ‘negative’ adjectives. I have chosen to try three classifying algorithms - original Naive Bayes, Logistic Regression and Support Vector classifiers. Then I provided the f1 accuracy scores.
### Needed Results
1. Normalized bag of words (BOW) of the corpus;
2. WordCloud for most positive adjectives found in the corpus;
3. Training set and testing set;
4. Metrics for each chosen classifier;
5. Most informative features with trained over the positive and negative words in the reviews.
### Result Evaluation
It seems that training was successful and all three of the chosen classifiers have given rather high accuracy score, taking into account that Sentiment Analyses usually don't yield as high accuracy as statistical analyses. For this reason, anything around 70% is a very good score. [2][3] The training models all achieved precision ranging from 77-79%. Confusion matrix of the true positive and true negative predictions is also quite good. The predictions of the words highlighted in the most informative features set have been very accurately labeled as either positive or negative; thus, this model can be used for doing sentiment analysis on different corpora.
### Artefacts
The job artefacts include the following files:
1. Figure 1: Wordcloud of positive words found in the corpus;
2. Figure 2: Confusion matrix non-normalized;
3. Figure 3: Confusion matrix normalized;
4. Some of the most informative feature examples with accurate predictions;
5. List of most frequent words.
### References
[1 | Sentiment Analysis Tutorial](https://towardsdatascience.com/imdb-reviews-or-8143fe57c825)
[2 | Research on Sentiment Analysis](https://www.researchgate.net/publication/300495226_Sentiment_Analysis)
[3 | Critical Review of Sentiment Analysis Techniques](https://www.researchgate.net/publication/265849484_Critical_Review_of_Sentiment_Analysis_Techniques)
