#!/usr/bin/env python

"""lab5 assignment"""
# dataset available from: http://ai.stanford.edu/~amaas/data/sentiment/
# followed a tutorial from: https://towardsdatascience.com/basic-binary-sentiment-analysis-using-nltk-c94ba17ae386
# importing the necessary modules and libraries
import os
import nltk
import random
from wordcloud import WordCloud
from sklearn.metrics import f1_score
from nltk.tokenize import word_tokenize
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix
from nltk.corpus import stopwords
import matplotlib.pyplot as plt
import numpy as np
import pickle
import re


def find_features(doc):
    """for creating a dictionary of features for each review"""
    words = word_tokenize(doc)
    features = {}
    for w in word_features:
        features[w] = (w in words)
    return features


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None):
    """plotting the confusion matrix"""
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

        # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only using the labels that appear in the data
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest')
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotating the tick labels and set their alignment
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Looping over data dimensions and create text annotations
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax


if __name__ == "__main__":
    # reading in corpus
    data_pos = os.listdir('data/train/pos')
    data_pos = [open('data/train/pos/' + f, 'r', encoding="utf8",
                     errors='ignore').read() for f in data_pos]
    data_neg = os.listdir('data/train/neg')
    data_neg = [open('data/train/neg/' + f, 'r', encoding="utf8",
                     errors='ignore').read() for f in data_neg]

    all_words = []
    docs = []
    # stopwords will be needed for text cleaning
    stop = list(set(stopwords.words('english')))

    allowed_word_types = ["J"]  # adjectives

    for p in data_pos:
        # creating a list of tuples where 1st element is a review, 2nd - label
        docs.append((p, "pos"))
        cleaned = re.sub(r'[^(a-zA-Z)\s]', '', p)  # removing punctuation marks
        tokenized = word_tokenize(cleaned)  # text tokenization
        # removing the stopwords
        removed = [w for w in tokenized if w not in stop]
        pos = nltk.pos_tag(removed)  # part of speech tagging for each word
        # making a list of all adjectives by the word types listed above
        for w in pos:
            if w[1][0] in allowed_word_types:
                all_words.append(w[0].lower())

    for p in data_neg:  # the same for the negative reviews
        docs.append((p, "neg"))
        cleaned = re.sub(r'[^(a-zA-Z)\s]', '', p)
        tokenized = word_tokenize(cleaned)
        removed = [w for w in tokenized if w not in stop]
        neg = nltk.pos_tag(removed)
        # list of adjectives
        for w in neg:
            if w[1][0] in allowed_word_types:
                all_words.append(w[0].lower())

    # creating a frequency distribution of each adjective
    all_words = nltk.FreqDist(all_words)
    # listing the most frequent words
    word_features = list(all_words.keys())[:1000]
    save_word_features = open("results/word_features5k.pickle", "wb")
    pickle.dump(word_features, save_word_features)
    save_word_features.close()

    # creating a word cloud of positive adjectives
    pos_cloud = []
    for w in pos:
        if w[1][0] in allowed_word_types:
            pos_cloud.append(w[0].lower())

    text = ' '.join(pos_cloud)
    word_cloud = WordCloud(width=1650, height=1250).generate(text)

    plt.figure(figsize=(15, 9))
    plt.imshow(word_cloud, interpolation="bilinear")
    plt.axis("off")
    plt.show()
    word_cloud.to_file('results/Figure_1_wordcloud.png')

    # creating features for each review
    feature_sets = [(find_features(rev), category) for (rev, category) in docs]
    random.shuffle(feature_sets)  # shuffling the documents randomly

    training_set = feature_sets[:5000]  # creating training and testing sets
    testing_set = feature_sets[5000:]

    # training with 3 classifiers
    NB_clf = nltk.NaiveBayesClassifier.train(training_set)  # Naive Bayes

    LR_clf = SklearnClassifier(LogisticRegression())  # Logistic Regression
    LR_clf.train(training_set)

    SVC_clf = SklearnClassifier(SVC())  # Support Vector Classifier
    SVC_clf.train(training_set)

    # accuracy of each model
    print("Naive Bayes Classifier accuracy %:",
          (nltk.classify.accuracy(NB_clf, testing_set))*100)
    print("LR Classifier accuracy %:",
          (nltk.classify.accuracy(LR_clf, testing_set)) * 100)
    print("SVC Classifier accuracy %:",
          (nltk.classify.accuracy(SVC_clf, testing_set)) * 100)

    # showing some of the trained features
    described_data = NB_clf.most_informative_features(20)
    print(described_data)
    # saving to results
    with open('results/most-informative-features.txt', 'w') as outfile:
        outfile.write('\n'.join(str(item) for item in described_data))

    # creating a confusion metrics to show the accuracy of prediction
    ground_truth = [r[1] for r in testing_set]
    predictions = [NB_clf.classify(r[0]) for r in testing_set]
    f1_score(ground_truth, predictions, labels=['neg', 'pos'], average='micro')

    np.set_printoptions(precision=2)

    y_test = ground_truth
    y_pred = predictions
    class_names = ['neg', 'pos']
    np.set_printoptions(precision=2)

    # Plotting non-normalized confusion matrix
    plot_confusion_matrix(y_test, y_pred, classes=class_names,
                          title='Confusion matrix, without normalization')
    plt.savefig('results/Figure2_Confusion_matrix_not_normalized.png')

    # Plotting normalized confusion matrix
    plot_confusion_matrix(y_test, y_pred, classes=class_names, normalize=True,
                          title='Normalized confusion matrix')
    plt.savefig('results/Figure3_Confusion_matrix_normalized.png')

    plt.show()

    print('Done')
